import os
import logging
from ..own_string.path import extract_dir, extract_file

"""
    Fonction file_exist
    %       Fonction qui nous indique si le fichier pointé par le
            chemin passé en argument existe
    %IN     path: chaine de caractère indiquant le chemin en question
    %OUT    out : booléen indiquant l'existance ou non du fichier
"""

def file_exist(path):
    logging.debug("Call extract_dir")
    directory = extract_dir(path)
    logging.debug("Call extract_file")
    bdd_name = extract_file(path)
    out = False
    logging.debug("Search File in Directory")
    for files in os.walk(directory):
        for i in range(len(files[2])):
            if bdd_name in files[2][i]:
                out = True
                return out
    return out