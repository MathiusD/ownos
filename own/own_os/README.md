# Module own_os

## Description

Ce module nous permet de rechercher l'existence d'un fichier.

### Fonctions

Dans ce module on possède qu'une fonction :

* file_exist() qui parcours le dossier parent d'un path et y recherche le fichier de ce path.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_os. On y teste notre fonction avec 5 lots de données.

### Dépendances

Ce module dépend du module constructeur logging, os et de notre module own_string.
