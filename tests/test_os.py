import unittest
import os
from own.own_os.verify import file_exist

class Test_Own_CSV(unittest.TestCase):

    def test_file_exist(self):
        path1 = "smirk/.sqlite"
        path2 = "tests/test_os.py"
        path3 = "tests/"
        path4 = "own/own_os/verify.py"
        path5 = ".zblep"
        self.assertEqual(False, file_exist(path1))
        self.assertEqual(True, file_exist(path2))
        self.assertEqual(True, file_exist(path3))
        self.assertEqual(True, file_exist(path4))
        self.assertEqual(False, file_exist(path5))